<?php
namespace Drupal\commerce_sage\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_payment\Entity\Payment;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Url;


class CommerceSage extends ControllerBase {

  public function Secure3DBack($order) {
    \Drupal::logger('postfirst')->notice(print_r($_POST, true));
    $payment_success = FALSE;
    $element = [];
    $configuration = getPaymentConfigurations();
    if(!empty($configuration['uri']) && !empty($configuration['auth'])) {
      $orderObj = Order::load($order);
      if(empty($orderObj)) {
        \Drupal::messenger()->addMessage("Specified Order not exists, please contact our administrator.", 'error');
        \Drupal::logger('SagePay Payment 3D error')->error('Order not exists MD: '. $_POST['MD']);
        return [];
      }
      if (isset($_POST['cres'])) {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $configuration['uri']['transactions'] . base64_decode($_POST['threeDSSessionData']) . '/3d-secure-challenge',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => '{"cRes": "' . $_POST['cres'] . '"}',
            CURLOPT_HTTPHEADER => array(
              "Authorization: Basic ". $configuration['auth'],
              "Cache-Control: no-cache",
              "Content-Type: application/json"
            ),
          )
        );
        $err = curl_error($curl);

        if(!empty($err)) {
          \Drupal::logger('SagePay Payment ERROR 3')->error(print_r($err, TRUE));
        }

        $response = curl_exec($curl);
        $response = json_decode($response);
        curl_close($curl);

        if ($response->statusCode == '0000' && $response->{'3DSecure'}->status == 'Authenticated') {
          $payment_success = TRUE;
        }

        \Drupal::logger('newResp')->notice(print_r($response, true));
        \Drupal::logger('newResp')->notice(print_r($response->{'3DSecure'}->status, true));
      }
      else{
        if(empty($_POST['MD'])) {
          \Drupal::messenger()->addMessage("3D secure MD is not exists, please contact our administrator", 'error');
          \Drupal::logger('SagePay Payment 3D error')->error('3D secure MD is not exists (orderID: '. $order. ')');
          return [];
        }

        if(empty($_POST['PaRes'])) {
          \Drupal::messenger()->addMessage("3D secure PaRes is not exists", 'error');
          \Drupal::logger('SagePay Payment 3D error')->error('3D secure PaRes is not exists (orderID: '. $order. ')(MD: ' . $_POST['MD'] . ')');
          return [];
        }

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $configuration['uri']['transactions'] . $_POST['MD'] . '/3d-secure',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => '{"paRes": "' . $_POST['PaRes'] . '"}',
            CURLOPT_HTTPHEADER => array(
              "Authorization: Basic ". $configuration['auth'],
              "Cache-Control: no-cache",
              "Content-Type: application/json"
            ),
          )
        );
        \Drupal::logger('postpares')->notice(print_r($_POST, true));
        $err = curl_error($curl);

        if(!empty($err)) {
          \Drupal::logger('SagePay Payment ERROR 3')->error(print_r($err, TRUE));
        }

        $response = curl_exec($curl);
        $response = json_decode($response);
        curl_close($curl);
        \Drupal::logger('postRea')->notice(print_r($response, true));

        if(!empty($response->status) && $response->status == 'Authenticated') {
          $payment_success = true;
        }
      }

      $data = $_POST;
      if (!empty($_POST['MD'])) {
        $data['transactionId'] = $_POST['MD'];
      } else {
        $data['transactionId'] = base64_decode($_POST['threeDSSessionData']);
      }

      if($payment_success) {

        $orderObj->setData('sagepay_form', [
          'request' => (object) $data,
        ]);
        $orderObj->set('state', 'completed');
        $orderObj->save();

        $payment_method = $orderObj->get('payment_method')->getValue();

        // Simulate the order being paid in full.
        $payment = Payment::create([
          'type' => 'payment_default',
          'payment_gateway' => 'commerce_sage',
          'payment_method' => $payment_method[0]['target_id'],
          'order_id' => $orderObj->id(),
          'amount' => $orderObj->getTotalPrice(),
          'state' => 'completed',

        ]);

        $payment->save();

        $url = Url::fromRoute('commerce_checkout.form', ['commerce_order' => $orderObj->id()])->toString();
        $response = new RedirectResponse($url);
        $response->send();
      }
      else {
        \Drupal::messenger()->addMessage("3D secure payment invalid, please contact our administrator.", 'error');

        \Drupal::logger('SagePay Payment 3D error')->error('configuration is not correct, TID: '. $data['transactionId']. ' Order: '. $order);
      }
    }
    else {
      \Drupal::messenger()->addMessage("Specified Order not exists, please contact our administrator.", 'error');
      \Drupal::logger('SagePay Payment 3D error')->error('configuration is not correct, Order ID: '. $order->id());
    }
    return $element;
  }
}
