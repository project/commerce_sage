<?php

namespace Drupal\commerce_sage\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\Entity\OrderItemInterface;
use Drupal\commerce_payment\CreditCard;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OnsitePaymentGatewayBase;
use Drupal\commerce_price\Price;
use Drupal\commerce_payment\Exception\HardDeclineException;
use Drupal\commerce_payment\Exception\DeclineException;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Provides the On-site payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "sagepay_onsite",
 *   label = "SagePay (On-site)",
 *   display_label = "SagePay",
 *   forms = {
 *     "add-payment-method" = "Drupal\commerce_sage\PluginForm\Onsite\PaymentMethodAddForm",
 *     "edit-payment-method" = "Drupal\commerce_payment\PluginForm\PaymentMethodEditForm",
 *   },
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {
 *     "amex", "maestro", "mastercard", "visa",
 *   },
 *   requires_billing_information = FALSE,
 * )
 */
class Onsite extends OnsitePaymentGatewayBase implements OnsiteInterface
{
  public $sagepayConfig;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, PaymentTypeManager $payment_type_manager, PaymentMethodTypeManager $payment_method_type_manager, TimeInterface $time) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $payment_type_manager, $payment_method_type_manager, $time);
  }


  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state)
  {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['vendor'] = [
      '#type' => 'textfield',
      '#title' => t('SagePay Vendor Name'),
      '#description' => t('This is the vendor name that SagePay sent you when
     you set up your account.'),
      '#required' => TRUE,
      '#default_value' => isset($this->configuration['vendor']) ? $this->configuration['vendor'] : '',
    ];

    $form['live_integration_key'] = [
      '#type' => 'textfield',
      '#title' => t('LIVE Integration Key'),
      '#default_value' => (isset($this->configuration['live_integration_key'])) ? $this->configuration['live_integration_key'] : '',
      '#required' => TRUE,
    ];

    $form['live_integration_password'] = [
      '#type' => 'textfield',
      '#title' => t('LIVE Integration Password'),
      '#default_value' => (isset($this->configuration['live_integration_password'])) ? $this->configuration['live_integration_password'] : '',
      '#required' => TRUE,
    ];

    $form['test_integration_key'] = [
      '#type' => 'textfield',
      '#title' => t('TEST Integration Key'),
      '#default_value' => (isset($this->configuration['test_integration_key'])) ? $this->configuration['test_integration_key'] : '',
      '#required' => TRUE,
    ];

    $form['test_integration_password'] = [
      '#type' => 'textfield',
      '#title' => t('TEST Integration Password'),
      '#default_value' => (isset($this->configuration['test_integration_password'])) ? $this->configuration['test_integration_password'] : '',
      '#required' => TRUE,
    ];


    $form['transaction'] = [
      '#type' => 'fieldset',
      '#title' => 'Transaction Settings',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    ];

    $form['transaction']['sagepay_order_description'] = [
      '#type' => 'textfield',
      '#title' => t('Order Description'),
      '#description' => $this->t('The description of the order that will appear in the SagePay transaction. (For example, Your order from sitename.com)'),
      '#default_value' => (isset($this->configuration['sagepay_order_description'])) ? $this->configuration['sagepay_order_description'] : $this->t('Your order from sitename.com'),
    ];

    $form['security'] = [
      '#type' => 'fieldset',
      '#title' => 'Security Checks',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    ];

    $form['security']['sagepay_apply_avs_cv2'] = [
      '#type' => 'radios',
      '#title' => t('AVS / CV2 Mode'),
      '#description' => t('CV2 validation mode used by default on all transactions.'),
      '#options' => [
        '0' => t('If AVS/CV2 enabled then check them. If rules apply, use rules. (default)'),
        '1' => t('Force AVS/CV2 checks even if not enabled for the account. If rules apply, use rules.'),
        '2' => t('Force NO AVS/CV2 checks even if enabled on account.'),
        '3' => t('Force AVS/CV2 checks even if not enabled for the account but DO NOT apply any rules.'),
      ],
      '#default_value' => (isset($this->configuration['sagepay_apply_avs_cv2'])) ? $this->configuration['sagepay_apply_avs_cv2'] : 0,
    ];


    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state)
  {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['vendor'] = $values['vendor'];
      $this->configuration['live_integration_key'] = $values['live_integration_key'];
      $this->configuration['live_integration_password'] = $values['live_integration_password'];
      $this->configuration['test_integration_key'] = $values['test_integration_key'];
      $this->configuration['test_integration_password'] = $values['test_integration_password'];
      $this->configuration['sagepay_order_description'] = $values['transaction']['sagepay_order_description'];
      $this->configuration['sagepay_apply_avs_cv2'] = $values['security']['sagepay_apply_avs_cv2'];
    }
  }

  /**
   * Return Data array with information for transaction
   * @param $payment_method
   * @param $order
   *
   * @return array
   */
  public function getPaymentData($payment_method, $order) {
    $payment_method_data = [];

    if ($billing_profile = $payment_method->getBillingProfile()) {
      $billing_address = $billing_profile->get('address')->first()->toArray();
      $payment_method_data['address'] = [
        'city' => $billing_address['locality'],
        'country' => $billing_address['country_code'],
        'line1' => $billing_address['address_line1'],
        'line2' => $billing_address['address_line2'],
        'postal_code' => $billing_address['postal_code'],
        'state' => $billing_address['administrative_area'],
      ];
      $payment_method_data['customerFirstName'] = $billing_address['given_name'];
      $payment_method_data['customerLastName'] = $billing_address['family_name'];
    }

    if(empty($payment_method_data['customerFirstName'])) {
      \Drupal::messenger()->addMessage('First Name is required', 'error');
      throw new PaymentGatewayException('ERROR First Name is required');
    }
    if(empty($payment_method_data['customerLastName'])) {
      \Drupal::messenger()->addMessage('Last Name is required', 'error');
      throw new PaymentGatewayException('ERROR Last Name is required');
    }

    $payment_method_data['amount'] = $order->getTotalPrice()->getNumber();
    $remoteId = $payment_method->getRemoteId();
    $remoteIdArray = explode(':', $remoteId);
    if(empty($remoteIdArray[0]) || empty($remoteIdArray[1])) {
      \Drupal::messenger()->addMessage('Merchant key or Card identifier is not correct');
      throw new PaymentGatewayException('ERROR Merchant key or Card identifier is not correct');
    }
    $payment_method_data['merchant_session_key'] = $remoteIdArray[0];
    $payment_method_data['card_identifier'] = $remoteIdArray[1];

    return $payment_method_data;
  }

  /**
   * get description from order items labels
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *
   * @return bool|string
   */
  protected function getDescriptionFromProducts(OrderInterface $order) {
    $items = $order->getItems();
    // Create basket from saved products.
    /** @var OrderItemInterface $item */
    $description = "";
    foreach ($items as $item) {
      $description = $description . " " . $item->label(). "(quantity: " . $item->getQuantity() ."),";
    }

    return substr($description, 0, -1);
  }

  /**
   * Get default(first) order by paymentID
   * @param $paymentId
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   */
  protected function getDefaultOrder($paymentId) {
    if(empty($paymentId)) {
      throw new \InvalidArgumentException("Payment Id is empty");
    }

    $order_storage = $this->entityTypeManager->getStorage('commerce_order');
    $order_ids = $order_storage->getQuery()
      ->condition('type', 'default')
      ->condition('state', 'completed')
      ->condition('payment_method', $paymentId)
      ->accessCheck(FALSE)
      ->execute();

    $default_order = NULL;
    foreach($order_ids as $order_id) {
      $default_order = $order_storage->load($order_id);
      break;
    }

    return $default_order;
  }

  /**
   * {@inheritdoc}
   * CreatePayment for sagepay also allow recuring payemnt by commerce_recuring module
   */
  public function createPayment(PaymentInterface $payment, $capture = TRUE)
  {
    $this->assertPaymentState($payment, ['new']);
    $payment_method = $payment->getPaymentMethod();
    $this->assertPaymentMethod($payment_method);

    $order = $payment->getOrder();

    //For sagepay recuring payment we allow commerce_recurring module to make repeat payment
    if ($order->bundle() == 'recurring') {

      //Get default order with same paymentId
      $payment_id = $payment_method->id();
      $default_order = $this->getDefaultOrder($payment_id);

      $old = FALSE;
      $transactionId = NULL;
      if (!empty($default_order)) {
        $data = $default_order->getData('sagepay_form');

        if (!empty($data['request']->transactionId)) {
          $transactionId = $data['request']->transactionId;
        }
        else {
          $data = $default_order->getData('sagepay');
          $txid = reset($data)['VPSTxId'];
          $txid = rtrim($txid, '}');
          $txid = trim($txid, '{');
          $transactionId = $txid;
          $old = TRUE;

        }
      }
      else {
        throw new DeclineException('There is no default completed order for order: ' . $order->id());
      }
      // If transaction Id is not exists we could make.
      $uid = $order->get('uid')->getValue()[0]['target_id'];
      $query = \Drupal::entityTypeManager()->getStorage('commerce_subscription')->loadByProperties([
        'uid' => $uid
      ]);

      $commerce_subscription = reset($query);
      if ($commerce_subscription->hasScheduledChanges()) {
        throw new DeclineException('The user has already scheduled cancellation of subscription');
      } else {
        if (!empty($transactionId)) {
          if(!$old) {
            $this->repeatTransaction($order, $payment, $transactionId);
          } else {
            $this->oldRepeatTransaction($order, $payment, $transactionId);
          }
        } else {
          throw new DeclineException('Default order does not have a transaction Id. Order: '. $order->id() . ', default order: ' . $default_order->id());
        }
      }
    } else {
      $this->newTransaction($order, $payment, $capture);
    }
  }

  /**
   * New Transaction request function
   * @param $order
   * @param $payment
   * @param bool $capture
   */
  public function newTransaction($order, $payment, $capture = TRUE) {
    $user = \Drupal::currentUser();
    if($user->id() == 0) {
      foreach ($order->getItems() as $order_item) {
        $mail = $order_item->getData('mail');
      }
    } else {
      $mail = $user->getEmail();
    }

    $payment_method = $payment->getPaymentMethod();
    $sagepayConfig = $this->getSagepayConfiguration($order);
    $payment_method_data = $this->getPaymentData($payment_method, $order);

    $curl = curl_init();

    //integer: The amount charged to the customer in the smallest currency unit.
    //need to change ammount to pence
    //TODO: hard coded pence value 100, need to be configurable
    $payment_method_data['amount'] = floatval($payment_method_data['amount']);
    $payment_method_data['amount'] = $payment_method_data['amount'] * 100;

    //Change default description value with order items labels
    $sagepayConfig['sagepay_order_description'] .= ', items:' . $this->getDescriptionFromProducts($order);

    $city = ' ';
    if (!empty($payment_method_data['address']['city'])) {
      $city = $payment_method_data['address']['city'];
    } else {
      $city = $payment_method_data['address']['country'];
    }
    $options = ['absolute' => TRUE];
    $url = Url::fromRoute('commerce_sage.3d_secure_page', ['order' => $order->id()], $options);
    $path = $url->toString();
    curl_setopt_array($curl, array(
      CURLOPT_URL => $sagepayConfig['uri']['transactions'],
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => '{' .
        '"transactionType": "Payment",' .
        '"paymentMethod": {' .
        '    "card": {' .
        '        "merchantSessionKey": "' . $payment_method_data['merchant_session_key'] . '",' .
        '        "cardIdentifier": "' . $payment_method_data['card_identifier'] . '",' .
        '        "save": "false"' .
        '    }' .
        '},' .
        '"vendorTxCode": "' . $payment->getOrderId()  .  time() . '",' .
        '"amount": ' . $payment_method_data['amount'] . ',' .
        '"currency": "' . $sagepayConfig['currency'] . '",' .
        '"description": "' . $sagepayConfig['sagepay_order_description'] . '",' .
        '"apply3DSecure": "UseMSPSetting",' .
        '"recurringIndicator" : "Instalment",' .
        '"customerFirstName": "' . $payment_method_data['customerFirstName'] . '",' .
        '"customerLastName": "' . $payment_method_data['customerLastName'] . '",' .
        '"billingAddress": {' .
        '    "address1": "' . $payment_method_data['address']['line1'] . '",' .
        '    "address2": "' . $payment_method_data['address']['line2'] . '",'.
        '    "city": "' . $city . '",' .
        '    "postalCode": "' . $payment_method_data['address']['postal_code'] . '",' .
        '    "country": "' . $payment_method_data['address']['country'] . '"' .
        '},' .
        '"credentialType": {' .
        '   "cofUsage": "First",' .
        '   "initiatedType": "CIT",' .
        '   "mitType": "Unscheduled"' .
        '},' .
        '"strongCustomerAuthentication": {' .
        '"notificationURL": "'.$path.'",' .
        '"browserIP": "'.\Drupal::request()->getClientIp() .'",' .
        '"browserUserAgent": "'.$_SERVER['HTTP_USER_AGENT'].'",' .
        '"browserJavascriptEnabled": "false",' .
        '"browserAcceptHeader": "application/json",' .
        '"browserLanguage": "en",' .
        '"challengeWindowSize": "Small"' .
        //        '"transType": "GoodsAndServicePurchase"' .
        '},' .
        '"entryMethod": "Ecommerce",' .
        '"customerEmail": "' . $mail . '"' .
        '}',
      CURLOPT_HTTPHEADER => array(
        "Authorization: Basic ". $sagepayConfig['auth'],
        "Cache-Control: no-cache",
        "Content-Type: application/json"
      ),
    ));

    $err = curl_error($curl);
    if(!empty($err)) {
      print_r($err);
      \Drupal::logger('SagePay Payment ERROR 1')->error(print_r($err, TRUE));
    }

    $response = curl_exec($curl);
    $header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
    $header = substr($response, 0, $header_size);

    $response = json_decode($response);

    curl_close($curl);
    \Drupal::logger('test')->notice(print_r($response, true));
    if(!empty($response->statusCode) && $response->statusCode == 2021 && !empty($response->transactionId) && !empty($response->cReq)) {
      echo '<form style="padding-top: 40px; text-align: center; background: #124666; width: calc(100% + 16px); height: 100%; margin: -8px;" id="myForm" action="' . $response->acsUrl . '" method="post">' .
        '<input type="hidden" name="creq" value="' . $response->cReq . '">' .
        '<input type="hidden" name="threeDSSessionData" value="' . base64_encode($response->transactionId) . '">' .
        '<p style="color: white">Redirecting to 3D secure authorization.</p>'.
        '<button style="width: auto;
    height: 40px; font-size: large; background-color: #ea8650;
    margin: 0; line-height: 0; color: white; box-shadow: none;
    border: none; border-radius: 3px; cursor: pointer;" type="submit" id="submitForm">Click here if not redirected in five seconds</button></form>'.
        '<script type="application/javascript">document.getElementById("submitForm").click();</script>';
      exit();
    } elseif(!empty($response->statusCode) && $response->statusCode == 2007 && !empty($response->transactionId) && !empty($response->paReq)) {


      echo '<form style="padding-top: 40px; text-align: center; background: #124666; width: calc(100% + 16px); height: 100%; margin: -8px;" id="myForm" action="' . $response->acsUrl . '" method="post">' .
        '<input type="hidden" name="MD" value="' . $response->transactionId . '">' .
        '<input type="hidden" name="TermUrl" value="' . $path . '">' .
        '<input type="hidden" name="PaReq" value="' . $response->paReq . '">' .
        '<p style="color: white">Redirecting to 3D secure authorization.</p>'.
        '<button style="width: auto;
    height: 40px; font-size: large; background-color: #ea8650;
    margin: 0; line-height: 0; color: white; box-shadow: none;
    border: none; border-radius: 3px; cursor: pointer;" type="submit" id="submitForm">Click here if not redirected in five seconds</button></form>'.
        '<script type="application/javascript">document.getElementById("submitForm").click();</script>';
      exit();
    } elseif (!empty($response->status) && $response->status == 'Ok' && $response->statusCode == '0000') {
      $order->setData('sagepay_form', [
        'request' => $response,
      ]);

      $order->save();
      $next_state = $capture ? 'completed' : 'authorization';
      $payment->setState($next_state);
      $payment->save();

    } elseif (!empty($response->errors)) {
      \Drupal::logger('SagePay Payment ERROR 2')->error(print_r($response, TRUE));
      $error_message = 'We encountered an unexpected error processing your payment method. Please try again later.';
      foreach($response->errors as $error){
        //Missing mandatory field
        if($error->code == '1003') {
          $error_message = $error->description . " " . $error->property;
          \Drupal::messenger()->addMessage($error_message, 'error');
        }
        throw new DeclineException($error_message);
      }
    } elseif (!empty($response->description)) {
      \Drupal::logger('SagePay Payment')->error($response->description . "code: " . $response->code);
      \Drupal::messenger()->addMessage($response->description, 'error');
      throw new PaymentGatewayException($response->description);
    } elseif (!empty($response->status) && $response->status == 'Invalid') {
      \Drupal::logger('SagePay Payment')->error($response->statusDetail . "StatusCode: " . $response->statusCode);
      throw new PaymentGatewayException($response->statusDetail);
    } else {
      throw new PaymentGatewayException("Sagepay getqay error, code" . (!empty($response->code) ? $response->code: ""));
    }
  }

  /**
   * Repeat transaction request function by first transaction ID
   * @param $order
   * @param $payment
   * @param $transactionId
   */
  public function repeatTransaction($order, $payment, $transactionId) {

    $sagepayConfig = $this->getSagepayConfiguration($order);
    $payment_method = $payment->getPaymentMethod();
    $payment_method_data = $this->getPaymentData($payment_method, $order);

    //TODO: hard coded pence value 100, need to be configurable
    $payment_method_data['amount'] = floatval($payment_method_data['amount']);
    $payment_method_data['amount'] = $payment_method_data['amount'] * 100;

    //Change default description value with order items labels
    $sagepayConfig['sagepay_order_description'] .= ', items:' . $this->getDescriptionFromProducts($order);

    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => $sagepayConfig['uri']['transactions'],
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => '{' .
        '"transactionType": "Repeat",' .
        '"referenceTransactionId": "' . $transactionId . '",' .
        '"vendorTxCode": "' . $payment->getOrderId()  .  time() . '",' .
        '"amount": ' . $payment_method_data['amount'] . ',' .
        '"currency": "' . $sagepayConfig['currency'] . '",' .
        '"description": "' . $sagepayConfig['sagepay_order_description'] . '",' .
        '"credentialType": {' .
        '   "cofUsage": "Subsequent",' .
        '   "initiatedType": "MIT",' .
        '   "mitType": "Unscheduled"' .
        '}' .
        '}',
      CURLOPT_HTTPHEADER => array(
        "Authorization: Basic ". $sagepayConfig['auth'],
        "Cache-Control: no-cache",
        "Content-Type: application/json"
      ),
    ));

    $err = curl_error($curl);
    if(!empty($err)) {
      \Drupal::logger('SagePay Payment ERROR 3')->error(print_r($err, TRUE));
    }

    $response = curl_exec($curl);
    $response = json_decode($response);
    curl_close($curl);

    if(!empty($response->status) && $response->status == 'Ok' && $response->statusCode == '0000') {
      $order->setData('sagepay_form', [
        'request' => $response,
      ]);

      if (!empty($order->getVersion())) {
        $order->setVersion($order->getVersion() + 1);
      }
      else {
        $order->setVersion(1);
      }

      $order->save();
      $payment->setState('completed');
      $payment->save();

    } elseif (!empty($response->errors)) {
      foreach($response->errors as $error){
        \Drupal::logger('SagePay Payment')->error($error->description . "code: " . $error->code);
        \Drupal::messenger()->addMessage($response->description, 'error');
        throw new DeclineException($response->description);
      }
    } elseif (!empty($response->description)) {
      \Drupal::logger('SagePay Payment')->error($response->description . "code: " . $response->code);
      \Drupal::messenger()->addMessage($response->description, 'error');
      throw new DeclineException($response->description);
    } elseif (!empty($response->status) && $response->status == 'Invalid') {
      \Drupal::logger('SagePay Payment')->error($response->statusDetail . "StatusCode: " . $response->statusCode);
      throw new DeclineException($response->statusDetail);
    } else {
      throw new DeclineException("Sagepay getqay error, code" . (!empty($response->code) ? $response->code: ""));
    }
  }

  public function oldRepeatTransaction($order, $payment, $transactionId) {

    $sagepayConfig = $this->getSagepayConfiguration($order);

    $amount = $order->getTotalPrice()->getNumber();
    //TODO: hard coded pence value 100, need to be configurable
    $amount = floatval($amount);
    $amount = $amount * 100;

    //Change default description value with order items labels
    $sagepayConfig['sagepay_order_description'] .= ', items:' . $this->getDescriptionFromProducts($order);

    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => $sagepayConfig['uri']['transactions'],
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => '{' .
        '"transactionType": "Repeat",' .
        '"referenceTransactionId": "' . $transactionId . '",' .
        '"vendorTxCode": "' . $payment->getOrderId()  .  time() . '",' .
        '"amount": ' . $amount . ',' .
        '"currency": "' . $sagepayConfig['currency'] . '",' .
        '"description": "' . $sagepayConfig['sagepay_order_description'] . '"' .
        '}',
      CURLOPT_HTTPHEADER => array(
        "Authorization: Basic ". $sagepayConfig['auth'],
        "Cache-Control: no-cache",
        "Content-Type: application/json"
      ),
    ));

    $err = curl_error($curl);
    if(!empty($err)) {
      \Drupal::logger('SagePay Payment ERROR 4')->error(print_r($err, TRUE));
    }

    $response = curl_exec($curl);
    $response = json_decode($response);
    curl_close($curl);

    if(!empty($response->status) && $response->status == 'Ok' && $response->statusCode == '0000') {
      $order->setData('sagepay_form', [
        'request' => $response,
      ]);

      $order->save();
      $payment->setState('completed');
      $payment->save();

    } elseif (!empty($response->errors)) {
      foreach($response->errors as $error){
        \Drupal::logger('SagePay Payment ERROR 5')->error(print_r($error));
        \Drupal::messenger()->addMessage($response->description, 'error');
        throw new DeclineException($response->description);
      }
    } elseif (!empty($response->description)) {
      \Drupal::logger('SagePay Payment')->error($response->description . "code: " . $response->code);
      \Drupal::messenger()->addMessage($response->description, 'error');
      throw new DeclineException($response->description);
    } elseif (!empty($response->status) && $response->status == 'Invalid') {
      \Drupal::logger('SagePay Payment')->error($response->statusDetail . "StatusCode: " . $response->statusCode);
      throw new DeclineException($response->statusDetail);
    } else {
      throw new DeclineException("Sagepay getqay error, code" . (!empty($response->code) ? $response->code: ""));
    }
  }

  /**
   * Get the Sagepay Configuration.
   *
   * @return \SagepayAbstractApi
   *   The Sagepay form api object.
   */
  protected function getSagepayConfiguration(OrderInterface $order = null)
  {
    if(!empty($this->sagepayConfig)) {
      return $this->sagepayConfig;
    }

    $gatewayConfig = $this->getConfiguration();
    if($gatewayConfig['mode'] == 'test') {
      $gatewayConfig['uri']['merchant_session_key'] = 'https://pi-test.sagepay.com/api/v1/merchant-session-keys/';
      $gatewayConfig['uri']['card_identifiers'] = 'https://pi-test.sagepay.com/api/v1/card-identifiers/';
      $gatewayConfig['uri']['transactions'] = 'https://pi-test.sagepay.com/api/v1/transactions/';
      $gatewayConfig['auth'] = base64_encode($gatewayConfig['test_integration_key'] . ":" . $gatewayConfig['test_integration_password']);
    } else {
      $gatewayConfig['uri']['merchant_session_key'] = 'https://pi-live.sagepay.com/api/v1/merchant-session-keys/';
      $gatewayConfig['uri']['card_identifiers'] = 'https://pi-live.sagepay.com/api/v1/card-identifiers/';
      $gatewayConfig['uri']['transactions'] = 'https://pi-live.sagepay.com/api/v1/transactions/';
      $gatewayConfig['auth'] = base64_encode($gatewayConfig['live_integration_key'] . ":" . $gatewayConfig['live_integration_password']);
    }
    $gatewayConfig['website'] = \Drupal::request()->getSchemeAndHttpHost();
    $gatewayConfig['siteFqdns'] = ['test' => \Drupal::request()->getSchemeAndHttpHost(), 'live' => \Drupal::request()->getSchemeAndHttpHost()];
    $gatewayConfig['currency'] = (!empty($order) ? $order->getTotalPrice()->getCurrencyCode() : null);
    $gatewayConfig['ApplyAVSCV2'] = $gatewayConfig['sagepay_apply_avs_cv2'];
    $this->sagepayConfig = $gatewayConfig;

    return $gatewayConfig;
  }

  /**
   * {@inheritdoc}
   */
  public function createPaymentMethod(PaymentMethodInterface $payment_method, array $payment_details)
  {

    $required_keys = [
      // The expected keys are payment gateway specific and usually match
      // the PaymentMethodAddForm form elements. They are expected to be valid.
      'number', 'expiration',
    ];
    foreach ($required_keys as $required_key) {
      if (empty($payment_details[$required_key])) {
        throw new \InvalidArgumentException(sprintf('$payment_details must contain the %s key.', $required_key));
      }
    }

    $user = \Drupal::currentUser();

    $billing_address = $payment_method->getBillingProfile()->get('address')->first()->toArray();
    $payment_method->getBillingProfile()->set('uid', $user->id());

    $payment_details['name'] = $billing_address['given_name'] . ' ' . $billing_address['family_name'];

    $merchant_session_key = $this->createMerchantSessionKey();
    $card_identifier = null;
    if(!empty($merchant_session_key->merchantSessionKey)) {
      $card_identifier = $this->createCardIdentifier($merchant_session_key->merchantSessionKey, $payment_details);

      // Only the last 4 numbers are safe to store.
      $payment_method->card_type = $this->mapCreditCardType($card_identifier->cardType);
      $payment_method->card_number = substr($payment_details['number'], -4);
      $payment_method->card_exp_month = $payment_details['expiration']['month'];
      $payment_method->card_exp_year = $payment_details['expiration']['year'];
      $expires = CreditCard::calculateExpirationTimestamp($payment_details['expiration']['month'], $payment_details['expiration']['year']);
      //    // The remote ID returned by the request.
      //    $remote_id = '789';
      //For continue payment we need to save merchant session key and card identifier
      $payment_method->setRemoteId($merchant_session_key->merchantSessionKey . ":" . $card_identifier->cardIdentifier);
      $payment_method->setExpiresTime($expires);
      $payment_method->setReusable(false);
      $payment_method->save();
    } else {
      throw new PaymentGatewayException("Unexpected error with your payment getway, please try later");
    }
  }


  /**
   * Create Merchant Session Key by sending request to sagepay
   * @return mixed
   */
  public function createMerchantSessionKey() {
    $sagepayConfig = $this->getSagepayConfiguration();
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => $sagepayConfig['uri']['merchant_session_key'],
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => '{ "vendorName": "' . $sagepayConfig['vendor'] . '" }',
      CURLOPT_HTTPHEADER => array(
        "Authorization: Basic " . $sagepayConfig['auth'],
        "Cache-Control: no-cache",
        "Content-Type: application/json"
      ),
    ));

    $response = curl_exec($curl);

    $err = curl_error($curl);
    if(!empty($err)) {
      \Drupal::logger('SagePay Payment ERROR 6')->error(print_r($err, TRUE));
    }

    $response = json_decode($response);
    curl_close($curl);

    if(!empty($response->merchantSessionKey)) {
      return $response;
    } else {
      \Drupal::logger('SagePay Payment ERROR 7')->error(print_r($response, TRUE));
      throw new PaymentGatewayException($response->description . "(code: " . $response->code . ")");
    }
  }

  /**
   * Create Card Identifier by sending already generated Merchant Session ID to Sagepay
   * @param $merchant_session_key
   * @param $cardDetails
   *
   * @return mixed
   */
  public function createCardIdentifier($merchant_session_key, $cardDetails) {
    $sagepayConfig = $this->getSagepayConfiguration();
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => $sagepayConfig['uri']['card_identifiers'],
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => '{' .
        '"cardDetails": {' .
        '    "cardholderName": "' . $cardDetails['name'] . '",' .
        '    "cardNumber": "' . $cardDetails['number'] . '",' .
        '    "expiryDate": "' . $cardDetails['expiration']['month'] .  substr( $cardDetails['expiration']['year'], -2) . '",' .
        '    "securityCode": "' . $cardDetails['security_code'] . '"' .
        '}' .
        '}',
      CURLOPT_HTTPHEADER => array(
        "Authorization: Bearer $merchant_session_key",
        "Cache-Control: no-cache",
        "Content-Type: application/json"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);
    if(!empty($err)) {
      \Drupal::logger('SagePay Payment ERROR 8')->error(print_r($err, TRUE));
    }

    $response = json_decode($response);
    curl_close($curl);

    if(!empty($response->cardIdentifier)) {
      return $response;
    } else {
      \Drupal::logger('SagePay Payment ERROR')->error(print_r($response, TRUE));
      throw new PaymentGatewayException($response->description . "(code: " . $response->code . ")");
    }
  }


  /**
   * {@inheritdoc}
   */
  public function capturePayment(PaymentInterface $payment, Price $amount = NULL)
  {
    $this->assertPaymentState($payment, ['authorization']);
    // If not specified, capture the entire amount.
    $amount = $amount ?: $payment->getAmount();

    $payment->setState('completed');
    $payment->setAmount($amount);
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function voidPayment(PaymentInterface $payment) {
    //TODO: sagepay void payment
  }

  /**
   * {@inheritdoc}
   */
  public function refundPayment(PaymentInterface $payment, Price $amount = NULL) {
    //TODO: sagepay refund payment
  }

  /**
   * {@inheritdoc}
   */
  public function deletePaymentMethod(PaymentMethodInterface $payment_method)
  {
    // Delete the remote record here, throw an exception if it fails.
    // See \Drupal\commerce_payment\Exception for the available exceptions.
    // Delete the local entity.
    $payment_method->delete();
  }

  /**
   * {@inheritdoc}
   */
  public function updatePaymentMethod(PaymentMethodInterface $payment_method)
  {
    // Note: Since requires_billing_information is FALSE, the payment method
    // is not guaranteed to have a billing profile. Confirm that
    // $payment_method->getBillingProfile() is not NULL before trying to use it.
    //
    // Perform the update request here, throw an exception if it fails.
    // See \Drupal\commerce_payment\Exception for the available exceptions.
  }

  /**
   * Maps the Sagepay credit card type to a Commerce credit card type.
   *
   * @param string $card_type
   *   The Stripe credit card type.
   *
   * @return string
   *   The Commerce credit card type.
   */
  protected function mapCreditCardType($card_type) {
    $map = [
      'amex' => 'amex',
      'AmericanExpress' => 'amex',
      'diners' => 'dinersclub',
      'Diners' => 'dinersclub',
      'discover' => 'discover',
      'jcb' => 'jcb',
      'mastercard' => 'mastercard',
      'MasterCard' => 'mastercard',
      'MC' => 'mastercard',
      'Visa' => 'visa',
      'visa' => 'visa',
      'Maestro' => 'maestro'
    ];
    if (!isset($map[$card_type])) {
      throw new HardDeclineException(sprintf('Unsupported credit card type "%s".', $card_type));
    }

    return $map[$card_type];
  }

}
